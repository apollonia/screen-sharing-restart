If you have a server Mac that runs headless, with no monitor connected you 
may find that when you restart it Screen Sharing no longer works. ARDTools 
started as a Python script that could be used to restart Screen Sharing. That 
script has now been refactored and packaged as ARDTools.

At the moment the Screen Sharing restart is the only command the package 
implements. I may add others as I find a need.